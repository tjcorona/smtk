## OpenCASCADE Session

SMTK now includes a minimal OpenCASCADE session; it can import
OpenCASCADE's native `.brep` files in addition to STEP and IGES
files. This session takes advantage of the new `smtk::graph::Resource`.
